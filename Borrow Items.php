<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Kanit:wght@400;700&display=swap');

        body {
            font-family: 'Kanit', sans-serif;
            margin: 0;
            padding: 0;
        }

        .logo {
            font-size: 25px;
            font-weight: bold;
            margin-left: 15px;
        }

        .logo img {
            width: 70px;
            height: auto;
        }

        /* ส่วนของ Navbar */
        nav {
            background-color: #56baed;
            padding: 10px 0;
            color: #56baed;
            /* display: flex; */
            /* align-items: center; Align items vertically */
        }

        ul.navbar-nav li.nav-item {
            display: inline;
            margin-right: 20px;
        }

        a.nav-link {
            text-decoration: none;
            color: #56baed;
        }

        a.nav-link:hover {
            text-decoration: underline;
        }

        /* ส่วนของ Card */
        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f0f0;
        }

        .dashboard {
            background-color: #4fc3f7;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
            width: 70%;
            max-width: 750px;
        }

        h1 {
            color: #333;
            margin-bottom: 20px;
            font-size: 20px;
        }

        .card {
            background-color: #f9f9f9;
            border-radius: 8px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            padding: 20px;
            margin-bottom: 20px;
        }

        .card h2 {
            color: #555;
            margin-top: 0;
        }

        .card p {
            color: #777;
            margin-bottom: 0;
        }

        .card:last-child {
            margin-bottom: 0;
        }

        .logo1 {
            font-size: 10px;
            font-weight: bold;
            margin-left: 15px;
        }

        .logo1 img {
            width: 7px;
            height: auto;
        }
        .icon-text-container {
        display: flex;
        align-items: center;
        }

        .icon {
        font-size: 20px;
        margin-right: 10px; /* เพื่อให้ระยะห่างระหว่างไอคอนกับข้อความ */
        }
    
    h2 {
        text-align: center;
        color: #333;
    }
    form {
        max-width: 400px;
        margin: 20px auto;
        background: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }
    label {
        font-weight: bold;
    }
    input[type="text"],
    input[type="date"],
    input[type="number"],
    input[type="submit"] {
        width: 100%;
        padding: 10px;
        margin: 5px 0;
        border: 1px solid #ccc;
        border-radius: 5px;
        box-sizing: border-box;
    }
    input[type="submit"] {
        background-color: #56baed;
        color: #fff;
        cursor: pointer;
    }
    input[type="submit"]:hover {
        background-color: #45a049;
    }

    </style>
     <!-- เพิ่มตำแหน่งนี้ -->
     <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>

</head>
<body>
<div class="logo"><img src="E-Borrow.png" alt="E-Borrow Logo"></div>

<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">หน้าหลัก</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">คลังสินค้า</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
         ยืม-คืน
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="Waiting to check.php">รอตรวจสอบ</a></li>
            <li><a class="dropdown-item" href="#">ไม่อนุมัติ</a></li>
            <li><a class="dropdown-item" href="approve.php">อนุมัติ</a></li>
            <li><a class="dropdown-item" href="Borrow Items.php">ทำรายการยืม</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">รายการครบกำหนดคืน</a></li>
            <li><a class="dropdown-item" href="report.php">ออกรายงาน</a></li>
          </ul>
        </li>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="s_logout.php">ออกจากระบบ</a>
        </li>
        </li>
    </div>
</nav>
<div class="icon-text-container">
  <span class="icon">📑ทำรายการยืม</h1></span>
</div>
<body>
    <h2>Borrow Item Form</h2>
    <form action="process_borrow.php" method="post">
    <!-- ตรวจสอบชื่อฟิลด์ของแต่ละองค์ประกอบ -->
    <label for="username">username</label><br>
    <input type="text" name="username">

    <label for="first_name">First Name:</label><br>
    <input type="text" name="first_name">

    <label for="last_name">Last Name:</label><br>
    <input type="text" name="last_name">

    <label for="list_name">list_name:</label><br>
    <input type="text" name="list_name">

    <label for="quantity">Quantity:</label><br> <!-- เพิ่มฟิลด์จำนวน -->
        <input type="number" id="quantity" name="quantity" min="1"><br> <!-- เพิ่มฟิลด์จำนวน -->

    <label for="borrow_date">Borrow Date:</label><br>
        <input type="date" id="borrow_date" name="borrow_date"><br>

        <label for="return_date">Return Date:</label><br>
        <input type="date" id="return_date" name="return_date"><br>

        <label for="purpose_use">Purpose of Use:</label><br>
        <input type="text" id="purpose_use" name="purpose_use"><br>

    <button type="submit">Submit</button>
</form>

</body>
</html>