<?php
if(isset($_POST['username']) && isset($_POST['password'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $authen_result = psupassport_authentication($username, $password);

    if ($authen_result["status"] == 1) {
        // print_r($authen_result);
        $ldapStaffID = $authen_result["result"][3];
        $ldapEmail = $authen_result["result"][13];
        $ldapUsername = $authen_result["result"][0];

        echo "<p>You are accessing<br>";
        echo "ID : $ldapStaffID<br>";
        echo "Username :$ldapUsername<br>";
        echo "Email : $ldapEmail<br>";
    }
    else {
        echo "Login Failed: Please check your username or password";
    }
}
else {
    echo "Plase enter username and password";
}

function psupassport_authentication($username, $password)
{
    $params = array('username' => "$username", 'password' => "$password");

    $url = "https://passport.psu.ac.th/authentication/authentication.asmx?WSDL";
    $client = new SoapClient($url, array("trace" => 1, "exceptions" => 0, "cache_wsdl" => 0));
    $result = $client->GetUserDetails($params);
    $authen_data = [];
    if (!is_a($result, "SoapFault")) {
        $authen_data = [
            "status" => true,
            "result" => $result->GetUserDetailsResult->string,
        ];
    } else {
        $authen_data = [
            "status" => false,
            "result" => $result->faultstring,
        ];
    }
    return $authen_data;
}
?>