<?php
include('db.php');
include('function.php');
if(isset($_POST["id"]))
{
	$output = array();
	$stmt = $connection->prepare(
		"SELECT * FROM equipment 
		WHERE id = :bp_id "
	);
	$stmt->bindParam(':bp_id', $_POST["id"]);
	$stmt->execute();
	$result = $stmt->fetchAll();
	foreach($result as $row)
	{
		$output["id"] = $row["id"];
		if($row["image"] != '')
		{
			$output['image'] = '<img src="upload/'.$row["image"].'" class="img-thumbnail" width="auto" height="auto" /><input type="hidden" name="hidden_image" value="'.$row["image"].'" />';
		}
		else
		{
			$output['image'] = '<input type="hidden" name="hidden_image" value="" />';
		}
		$output["list_name"] = $row["list_name"];
		$output["type"] = $row["type"];
		$output["quantity"] = $row["quantity"];
	}
	echo json_encode($output);
}
?>