-- MariaDB dump 10.19  Distrib 10.4.32-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: project
-- ------------------------------------------------------
-- Server version	10.4.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin1','Kanintra','Inprayad','123456');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrowing_returning`
--

DROP TABLE IF EXISTS `borrowing_returning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrowing_returning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `list_name` varchar(150) NOT NULL,
  `quantity` int(11) NOT NULL,
  `borrow_date` date NOT NULL,
  `return_date` date NOT NULL,
  `purpose_use` varchar(150) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrowing_returning`
--

LOCK TABLES `borrowing_returning` WRITE;
/*!40000 ALTER TABLE `borrowing_returning` DISABLE KEYS */;
INSERT INTO `borrowing_returning` VALUES (1,'6450110004','ซารีณา','ชูจันทร์','แก้วก้าน',5,'2024-03-24','2024-03-25','งานเลี้ยง',''),(2,'6450110004','ซารีณา','ชูจันทร์','แก้วก้าน',5,'2024-03-24','2024-03-30','ประชุม',''),(3,'6450110004','ซารีณา','ชูจันทร์','แก้วก้าน',180,'2024-03-24','2024-03-31','ประชุม','คืนแล้ว'),(4,'6450110004','ซารีณา','','แก้วน้ำ (แบบสูง)',5,'2024-03-24','2024-03-30','ประชุม','คืนแล้ว'),(5,'6450110004','ซารีณา','ชูจันทร์','จานรองแก้ว(เหล็ก)',30,'2024-03-24','2024-03-31','งานเลี้ยง','คืนแล้ว'),(6,'6450110004','ซารีณา','ชูจันทร์','แก้วก้าน',20,'2024-03-24','2024-03-31','งานเลี้ยง','คืนแล้ว'),(7,'6450110004','ซารีณา','ชูจันทร์','แก้วน้ำ (welcome drink เบอร์ 1)',55,'2024-03-16','2024-03-28','ประชุม','รอดำเนินการ'),(8,'6450110004','ซารีณา','ชูจันทร์','จานรองแก้ว(สีชมพู)',167,'2024-03-09','2024-03-30','งานเลี้ยง','รอดำเนินการ'),(9,'6450110004','ซารีณา','ชูจันทร์','แก้วก้าน',180,'2024-03-24','2024-03-30','ประชุม','รอดำเนินการ'),(10,'6450110008','คณินทรา','อินทร์ประหยัด','แก้วน้ำ 280 ml',20,'2024-03-24','2024-03-30','ประชุม','คืนแล้ว'),(11,'6450110008','คณินทรา','อินทร์ประหยัด','จานลายดอกสีแดง',111,'2024-03-24','2024-03-30','ประชุม','อนุมัติแล้ว'),(12,'6450110008','คณินทรา','อินทร์ประหยัด','จานลายดอกสีแดง',111,'2024-03-24','2024-03-30','งานเลี้ยง','รอดำเนินการ');
/*!40000 ALTER TABLE `borrowing_returning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(150) NOT NULL,
  `list_name` varchar(150) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` VALUES (1,'1945599456.jpg','แก้วก้าน','แก้ว',180),(2,'721187829.jpg','แก้วน้ำ (แบบสูง)','แก้ว',30),(3,'1027123066.jpg','แก้วน้ำ 280 ml','แก้ว',34),(4,'768239253.jpg','แก้วน้ำ (welcome drink เบอร์ 1)','แก้ว',55),(5,'688481033.jpg','แก้วน้ำ (welcome drink เบอร์ 2)','แก้ว',93),(6,'1217202386.jpg','จานรองแก้ว(เหล็ก)','จาน',40),(7,'700160526.jpg','จานรองแก้ว(แบบใส)','จาน',73),(8,'1937680869.jpg','จานรองแก้ว(สีชมพู)','จาน',167),(9,'226384953.jpg','จานลายดอกสีแดง','จาน',111),(10,'1303739971.jpg','จาน(สีฟ้า,ชมพู)','จาน',115),(11,'','จานวงรี(เบอร์ 1)','จาน',5),(12,'','จานวงรี(เบอร์ 2)','จาน',11),(13,'','จานกระเบื้องลายขนาดกลาง','จาน',6),(14,'','จานกระเบื้องลายดอกไม้','จาน',30),(15,'','จานผลไม้สีชมพู','จาน',58),(16,'','จานขนมขอบทอง','จาน',61),(17,'','จานขนมลายดอกไม้','จาน',40),(18,'','จานวงรี (สีฟ้า,สีชมพู)','จาน',38),(19,'','จานขนมสีขาวทรงช้อน	','จาน',48),(20,'','ถ้วยแกงลายดอกไม้ (เบอร์ 1)','ถ้วย',15),(21,'','ถ้วยแกงลายดอกไม้ (เบอร์ 2)','ถ้วย',12),(22,'','ถ้วยแกงลายดอกไม้ (เบอร์ 3)','ถ้วย',12),(23,'','ถ้วยแกงลายดอกไม้ (เบอร์ 4)','ถ้วย',12),(24,'','ถ้วยน้ำจิ้มสีชมพู','ถ้วย',47),(25,'','ถ้วยน้ำจิ้มสีน้ำตาล','ถ้วย',60),(26,'','ถ้วยน้ำจิ้มลายดอกไม้','ถ้วย',30),(27,'','ถ้วยน้ำจิ้มแบบใส','ถ้วย',120),(28,'','ถ้วยขนมหวานลายดอกไม้ (เบอร์ 1)','ถ้วย',10),(29,'','ถ้วยขนมหวานลายดอกไม้ (เบอร์ 2)','ถ้วย',15),(30,'','ถ้วยขนมหวานแบบใส','ถ้วย',14),(31,'','ถ้วยแกง (สีฟ้า,สีชมพู)','ถ้วย',82),(32,'','ถ้วยขนมหวานสีชมพู','ถ้วย',68),(33,'','ช้อน','ช้อน',129),(34,'','ส้อม','ช้อน',102),(35,'','ช้อนกลาง','ช้อน',110),(36,'','ช้อนกลางยาว','ช้อน',11),(37,'','ส้อมผลไม้','ช้อน',72),(38,'','ช้อนกาแฟ','ช้อน',149),(39,'','ทัพพีตักแกงด้ามดำ','ช้อน',6),(40,'','ทัพพีตักแกงด้ามน้ำตาล','ช้อน',2),(41,'','ทัพพีตักข้าว','ช้อน',13),(42,'','ทัพพีตักแกงด้ามสแตนเลส','ช้อน',2),(43,'','ตะหลิว','ช้อน',1),(44,'','ทัพพีแบบรู','ช้อน',1),(45,'','ทัพพีตักแกงด้ามสแตนเลสขนาดเล็ก','ช้อน',2),(46,'','ทัพพีตักข้าวขนาดใหญ่','ช้อน',4),(47,'','ช้อนตักน้ำแข็ง','ช้อน',1),(48,'','ชุดกาแฟขอบทอง	','ชุดกาแฟ',58),(49,'','ชุดกาแฟลายดอกไม้','ชุดกาแฟ',37),(50,'','ชุดกาแฟสีขาว','ชุดกาแฟ',96),(51,'','ชุดกาแฟสีชมพู','ชุดกาแฟ',76),(52,'','ชุดกาแฟสีขาวทรงช้อน','ชุดกาแฟ',48),(53,'','เหยือกน้ำ','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',4),(54,'','เหยือกน้ำแก้ว','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',3),(55,'','เหยือกน้ำสูญญากาศใหญ่','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',2),(56,'','เหยือกน้ำสูญญากาศเล็ก','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',2),(57,'','กระติกน้ำแข็ง','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',1),(58,'','กระติกน้ำร้อน','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',4),(59,'','คูลเลอร์น้ำร้อนที่เปิดน้ำเสียแล้ว','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',1),(60,'','คูลเลอร์น้ำ','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',7),(61,'','กระบอกน้ำ','เหยือกน้ำ/กระติกน้ำ/คูลเลอร์น้ำ',85),(62,'','ถาดเสริฟอาหารสแตนเลส','ถาดเสิร์ฟ',11),(63,'','ถาดเสริฟอาหารสีครีม','ถาดเสิร์ฟ',2),(64,'','ถาดเสริฟอาหารสีน้ำตาลเล็ก','ถาดเสิร์ฟ',2),(65,'','ถาดเสริฟอาหารสีน้ำตาลใหญ่','ถาดเสิร์ฟ',6),(66,'','ถาดเสริฟอาหารแบบกลม','ถาดเสิร์ฟ',4),(67,'','โถข้าวเซรามิก','โถข้าว',2),(68,'','โถข้าวธรรมดา (ฝาหาย)','โถข้าว',1),(69,'','กะละมังสแตนเลสล้างจานใหญ่	','กะละมัง/หม้อ',1),(70,'','กะละมังสแตนเลสล้างจานเล็ก','กะละมัง/หม้อ',1),(71,'','กะละมังพลาสติกสีดำล้างจานเล็ก','กะละมัง/หม้อ',2),(72,'','กะละมังพลาสติกสีดำล้างจานใหญ่','กะละมัง/หม้อ',2),(73,'','หม้อสแตนเลส','กะละมัง/หม้อ',1),(74,'','กะละมังสแตนเลส','กะละมัง/หม้อ',2),(75,'','กะบะใส่แก้วสีแดง','กระบะใส่แก้ว',8),(76,'','กะบะใส่แก้วสีน้ำเงิน','กระบะใส่แก้ว',5),(77,'','ชุดอ่างอุ่นอาหารขนาดใหญ่','ชุดอ่างอุ่นอาหาร/ถังแก็ส',3),(78,'','ชุดอ่างอุ่นอาหารขนาดเล็ก','ชุดอ่างอุ่นอาหาร/ถังแก็ส',3),(79,'','ถังแก๊ส','ชุดอ่างอุ่นอาหาร/ถังแก็ส',1),(80,'','กาน้ำชาใหญ่','กาน้ำชา',2);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-24 19:27:53
