<?php
include('db.php');
include('function.php');
if(isset($_POST["id"]))
{
	$output = array();
	$stmt = $connection->prepare(
		"SELECT * FROM borrowing_returning
		WHERE id = :bp_id "
	);
	$stmt->bindParam(':bp_id', $_POST["id"]);
	$stmt->execute();
	$result = $stmt->fetchAll();
	foreach($result as $row)
	{
		$output["id"] = $row["id"];
		$output["username"] = $row["username"];
		$output["first_name"] = $row["first_name"];
		$output["last_name"] = $row["last_name"];
		$output["list_name"] = $row["list_name"];
		$output["quantity"] = $row["quantity"];
		$output["borrow_date"] = $row["borrow_date"];
		$output["return_date"] = $row["return_date"];
		$output["purpose_use"] = $row["purpose_use"];
		$output["status"] = $row["status"];
	}
	echo json_encode($output);
}
?>