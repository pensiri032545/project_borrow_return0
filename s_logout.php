﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Logout</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f9f9f9;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .container {
            text-align: center;
            background-color: #4fc3f7;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        a {
            text-decoration: none;
            color: #f9f9f9
        }

        a:hover {
            text-decoration: underline;
        }
		
    </style>
</head>
<body>
<div class="container">
    <?php
    if(isset($_SESSION['login'])) {
        unset($_SESSION['login']);
        unset($_SESSION['uname']);
        $_SESSION = array(); // reset session array
        session_destroy();   // Finally, destroy the session.
        echo "<h2>You have successfully logged out.</h2>";
        echo "<p>Please <a href=\"welcome.php\">login</a>.</p>";
    }
    else {
       
        echo "<p>Please <a href=\"welcome.php\">logout</a>.</p>";
    }
    ?>
</div>
</body>
</html>