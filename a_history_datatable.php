<?php
include('db.php');
include('./a_history_function.php');

$query = "SELECT * FROM borrowing_returning";

if(isset($_POST["order"]))
{
    $query .= ' ORDER BY '.$_POST['order'][0]['column'].' '.$_POST['order'][0]['dir'];
}
else
{
    $query .= ' ORDER BY id ASC';
}

if($_POST["length"] != -1)
{
    $query .= ' LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$stmt = $connection->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll();

$data = array();
$filtered_rows = $stmt->rowCount();

foreach($result as $row)
{
    $sub_array = array();
    $sub_array[] = $row["id"];
    $sub_array[] = $row["username"];
    $sub_array[] = $row["first_name"];
    $sub_array[] = $row["last_name"];
    $sub_array[] = $row["list_name"];
    $sub_array[] = $row["quantity"];
    $sub_array[] = $row["borrow_date"];
    $sub_array[] = $row["return_date"];
    $sub_array[] = $row["purpose_use"];
    $sub_array[] = $row["status"];
    $data[] = $sub_array;
}

$output = array(
    "draw"              =>  intval($_POST["draw"]),
    "recordsTotal"      =>  get_total_all_records(),
    "recordsFiltered"   =>  $filtered_rows,
    "data"              =>  $data
);

echo json_encode($output);
?>
