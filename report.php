<?php
require_once __DIR__ . '/vendor/autoload.php';

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/tmp',
    ]),
    'fontdata' => $fontData + [
        'sarabun' => [
            'R' => 'THSarabunNew.ttf',
            'I' => 'THSarabunNew Italic.ttf',
            'B' => 'THSarabunNew Bold.ttf',
            'BI' => 'THSarabunNew BoldItalic.ttf' 
        ]
    ],
    'default_font' => 'sarabun'
]);

ob_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>รายงานการยืม-คืน</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Sarabun&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: 'Sarabun', sans-serif;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        table, th, td {
            border: 1px solid #000;
            padding: 8px;
            text-align: center;
        }
        th {
            background-color: #f2f2f2;
            font-size: 18px;
        }
        td {
            
            font-size: 18px;
            
        }
    
        h1 {
            text-align: center;
            margin-bottom: 30px;
       
        }
    </style>
</head>
<body>
<?php
$html = ob_get_clean();

// ตรวจสอบการเชื่อมต่อฐานข้อมูล
$host = "localhost";
$user = "root";
$passwd = "";
$dbname = "project";

// สร้างการเชื่อมต่อฐานข้อมูล
$conn = new mysqli($host, $user, $passwd, $dbname);

// ตรวจสอบการเชื่อมต่อ
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// เตรียมคำสั่ง SQL สำหรับการดึงข้อมูลการยืม
$sql = "SELECT * FROM borrowing_returning";

// ทำการค้นหาข้อมูล
$result = $conn->query($sql);

// สร้างรายงาน HTML จากข้อมูลในฐานข้อมูล
$html .= "<h1>รายงานการยืม-คืน </h1>";
if ($result->num_rows > 0) {
    $html .= "<table>";
    $html .= "<tr><th>รหัสนักศึกษา</th><th>ชื่อ-นามสกุล</th><th>รายการ</th><th>จำนวน</th><th>วันที่ยืม</th><th>วันที่คืน</th><th>วัตถุประสงค์</th></tr>";
    while ($row = $result->fetch_assoc()) {
        $html .= "<tr>";
        $html .= "<td>" . $row["username"] . "</td>";
        $html .= "<td>" . $row["first_name"] . " " . $row["last_name"] . "</td>";
        $html .= "<td>" . $row["list_name"] . "</td>";
        $html .= "<td>" . $row["quantity"] . "</td>";
        $html .= "<td>" . $row["borrow_date"] . "</td>";
        $html .= "<td>" . $row["return_date"] . "</td>";
        $html .= "<td>" . $row["purpose_use"] . "</td>";
        
        $html .= "</tr>";
    }
    $html .= "</table>";
} else {
    $html .= "<p>ไม่พบข้อมูลการยืม</p>";
}

// ปิดการเชื่อมต่อฐานข้อมูล
$conn->close();

$html .= "</body></html>";

// สร้างไฟล์ PDF จาก HTML
$mpdf->WriteHTML($html);

// กำหนดชื่อไฟล์ PDF และแสดงให้ดาวน์โหลด
$mpdf->Output('รายงานการยืม.pdf', 'D');
?>
