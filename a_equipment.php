<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Kanit:wght@400;700&display=swap');

        body {
            font-family: 'Kanit', sans-serif;
            margin: 0;
            padding: 0;
        }

        .logo {
            font-size: 25px;
            font-weight: bold;
            margin-left: 15px;
        }

        .logo img {
            width: 70px;
            height: auto;
        }

        /* ส่วนของ Navbar */
        nav {
            background-color: #56baed;
            padding: 10px 0;
            color: #56baed;
            /* display: flex; */
            /* align-items: center; Align items vertically */
        }

        ul.navbar-nav li.nav-item {
            display: inline;
            margin-right: 20px;
        }

        a.nav-link {
            text-decoration: none;
            color: #56baed;
        }

        a.nav-link:hover {
            text-decoration: underline;
        }
		</style>
     <!-- เพิ่มตำแหน่งนี้ -->
     <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>

</head>

<body>
<div class="logo"><img src="E-Borrow.png" alt="E-Borrow Logo"></div>

<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="a_dashbord.php">หน้าหลัก</a>
        </li>
    
        <li class="nav-item">
          <a class="nav-link" href="report.php"> ออกรายงาน</a>
        </li>
       
          
          <li class="nav-item">
          <a class="nav-link" href="s_logout.php">ออกจากระบบ</a>
        </li>
        </li>
    </div>
</nav>
</head>


<!DOCTYPE html>
<html>
<head>
	<title>ระบบยืม - คืนอุปกรณ์</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/2.0.1/css/dataTables.bootstrap5.css">
</head>
<style>

body {
            font-family: 'Kanit', sans-serif;
            margin: 0;
            padding: 0;
        }
</style>
<body>
	<div class="container box">
		<h1 align="center">รายการอุปกรณ์</h1>
		<br/>
		<div class="table-responsive">
		<br/>
			<div align="right">
    			<button type="button" id="add_button" class="btn btn-info">+ เพิ่มรายการอุปกรณ์</button>
   			</div>
			<table id="user_data" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th width="5%">ลำดับ</th>
						<th width="15%">รูปภาพ</th>
						<th width="30%">ชื่อรายการอุปกรณ์</th>
						<th width="20%">ประเภท</th>
						<th width="10%">จำนวน</th>
						<th width="10%">แก้ไขข้อมูล</th>
						<th width="10%">ลบข้อมูล</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<div id="equipmentModal" class="modal fade">
		<div class="modal-dialog">
			<form method="post" id="equipment_form" enctype="multipart/form-data">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">แก้ไขข้อมูลอุปกรณ์</h4>
					</div>
					<div class="modal-body">
						<label>ชื่อรายการอุปกรณ์</label>
						<input type="text" name="list_name" id="list_name" class="form-control" />
						<br />
						<label>ประเภท</label>
						<input type="text" name="type" id="type" class="form-control" />
						<br />
						<label>จำนวน</label>
						<input type="number" name="quantity" id="quantity" class="form-control" />
						<br />
						<label>เลือกรูปภาพ</label>
						<input type="file" name="image" id="image" />
						<span id="uploaded_image"></span>
					</div>
					<div class="modal-footer">
    					<input type="hidden" name="id" id="id" />
    					<input type="hidden" name="operation" id="operation" />
    					<input type="submit" name="action" id="action" class="btn btn-success" value="update"/>
    					<button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.7.1.js"></script>
	<script src="https://cdn.datatables.net/2.0.1/js/dataTables.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdn.datatables.net/2.0.1/js/dataTables.bootstrap5.js"></script>

	<script>
		// jQuery DataTable ดึงข้อมูลจากไฟล์ PHP ผ่าน Ajax แสดงข้อมูลในรูปแบบตาราง
		$(document).ready(function(){
		var dataTable = $('#user_data').DataTable({
			"processing":true,
			"serverSide":true,
			"order":[],
			"ajax":{
				url:"./a_datatable_fetch.php",
				type:"POST"
			},
			"columnDefs":[
				{
					"targets":[0, 3, 4],
					"orderable":false,
				},
			],
		});
	
		//
		$('#add_button').click(function(){
			$('#equipmentModal').modal('show'); 
			$('#list_name').val(''); 
			$('#type').val(''); 
			$('#quantity').val('');
			$('.modal-title').text("เพิ่มรายการอุปกรณ์"); 
			$('#uploaded_image').html('');
			$('#action').val('Create'); 
			$('#operation').val("Create");
			$('#equipmentModal').on('click', '.modal-footer button[data-dismiss="modal"]', function(){
				$('#equipmentModal').modal('hide');
			});
 		});

		//
		$('#action').click(function(){
		var listName = $('#list_name').val(); 
		var type = $('#type').val(); 
		var id = $('#id').val(); 
			if(listName != '' && type != '') {
				$.ajax({
					url : "./a_insert_update.php",    
					method:"POST",    
					data:{listName:listName, type:type, id:id, action:action}, 
					success:function(data){
						alert(data);    
						$('#equipmentModal').modal('hide'); 
						fetchUser();    
					}
				});
			} else 	{
					alert("กรุณาระบุข้อมูลอุปกรณ์"); 
				}
		});

		// เปิดโมดัล edit equipment เมื่อคลิกปุ่ม "update"
		$(document).on('submit', '#equipment_form', function(event){
		event.preventDefault();
		var formData = new FormData(this);
			$.ajax({
				url:"./a_insert_update.php",
				method:'POST',
				data:formData,
				contentType:false,
				processData:false,
				success:function(data)
				{
					alert(data);
					$('#equipment_form')[0].reset();
					$('#equipmentModal').modal('hide');
					dataTable.ajax.reload();
				}
			});
		});

		// แก้ไขรายการเมื่อคลิกปุ่ม "update"
		$(document).on('click', '.update', function(){
		var id = $(this).attr("id");
			$.ajax({
				url:"./a_update_fetch.php",
				method:"POST",
				data:{id:id},
				dataType:"json",
				success:function(data)
				{
					$('#equipmentModal').modal('show');
					$('#list_name').val(data.list_name);
					$('#type').val(data.type);
					$('#quantity').val(data.quantity);
					$('.modal-title').text("แก้ไขข้อมูลอุปกรณ์");
					$('#id').val(id);
					$('#uploaded_image').html(data.image);
					$('#action').val("Update");
					$('#operation').val("Update");
				}
			});
		
    	// เพิ่มการปิดโมดัลเมื่อคลิกปุ่ม "Close"
		$('#equipmentModal').on('click', '.modal-footer button[data-dismiss="modal"]', function(){
			$('#equipmentModal').modal('hide');
			});
		});

		// ลบรายการเมื่อคลิกปุ่ม "delete"
		$(document).on('click', '.delete', function(){
		var id = $(this).attr("id");
			if(confirm("คุณแน่ใจหรือไม่ว่าต้องการลบสิ่งนี้")){
			$.ajax({
				url:"./a_delete.php",
				method:"POST",
				data:{id:id},
					success:function(data)
					{
						$('#action').val("Delete");
						$('#operation').val("Delete");
						alert(data);
						dataTable.ajax.reload();
					}
				});
			} else 	{
						return false;	
				 	}
		});
	});
	</script>
</body>
</html>
