<?php
$host = "localhost";
$user = "root";
$passwd = "";
$dbname = "project";

try {
    $conn = new mysqli($host, $user, $passwd, $dbname);
    //echo "Connected successfully";
} catch(mysqli_sql_exception $e) {
    echo "Connection failed: " . $e->getMessage();
}