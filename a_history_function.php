<?php
include('db.php');

// ฟังก์ชันอัพโหลดรูปภาพ
function upload_image()
{
    if(isset($_FILES["image"]))
    {
        $extension = explode('.', $_FILES['image']['name']);
        $new_name = rand() . '.' . $extension[1];
        $destination = './upload/' . $new_name;
        move_uploaded_file($_FILES['image']['tmp_name'], $destination);
        return $new_name;
    }
}

// ฟังก์ชันในการรับชื่อรูปภาพ
function get_image_name($id)
{
    global $connection;
    $stmt = $connection->prepare("SELECT image FROM equipment WHERE id = :bp_id");
    $stmt->bindParam(':bp_id', $id);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result['image'];
}

// ฟังก์ชันในการคืนค่าจำนวนรายการทั้งหมด
function get_total_all_records()
{
    global $connection;
    $stmt = $connection->prepare("SELECT * FROM borrowing_returning");
    $stmt->execute();
    return $stmt->rowCount();
}
?>
