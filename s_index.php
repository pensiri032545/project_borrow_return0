﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Login LDAP</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Kanit:wght@400;700&display=swap');

        /* ใช้ฟอนต์ Kanit ทั้งหมด */
        body, input, label {
            font-family: 'Kanit', sans-serif;
        }
        
        body {
            background-color: #56baed; /* สีพื้นหลังของหน้าเว็บ */
            margin: 0; /* กำหนดให้ไม่มีระยะห่างข้างนอกขอบหน้าเว็บ */
            padding: 0; /* กำหนดให้ไม่มีระยะห่างภายในขอบหน้าเว็บ */
            display: flex;
            justify-content: center; /* Center horizontally */
            align-items: center; /* Center vertically */
            flex-direction: column; /* Align items vertically */
            height: 100vh; /* Full height of the viewport */
        }
        
        h1 {
            color: white; /* สีตัวอักษรของหัวข้อ */
            text-align: center; /* การจัดวางข้อความในรูปแบบกึ่งกลาง */
            margin-top: 50px; /* ระยะห่างด้านบนของหัวข้อ */
        }
        
        form {
            background-color: #ffffff; /* สีพื้นหลังของฟอร์ม */
            padding: 20px; /* ระยะห่างขอบฟอร์ม */
            border-radius: 10px; /* กำหนดขอบมน */
            width: 350px; /* กำหนดความกว้างของฟอร์ม */
            margin: 0 auto; /* การจัดกลางฟอร์มในหน้าเว็บ */
            margin-top: 20px; /* ระยะห่างด้านบนของฟอร์ม */
        }
        
        label {
            font-weight:normal; /* ทำให้ข้อความใน label เป็นตัวหนา */
        }
        
        input[type="text"],
        input[type="password"],
        input[type="submit"] {
            width: 100%; /* กำหนดให้ input เต็มขอบฟอร์ม */
            padding: 7px; /* ระยะห่างภายใน input */
            margin-bottom: 10px; /* ระยะห่างระหว่าง input */
            border: 1px solid #ccc; /* เส้นขอบ input */
            border-radius: 3px; /* กำหนดขอบมน */
        }
        
         input[type="submit"] {
            width: 312px; /* กำหนดความกว้างของฟอร์ม */
            margin: 0 auto; /* การจัดกลางฟอร์มในหน้าเว็บ */
            
            background-color: #56baed; /* สีพื้นหลังปุ่ม submit */
            color: #ffffff; /* สีตัวอักษรของปุ่ม submit */
            cursor: pointer; /* เปลี่ยนรูปเมาส์เป็นรูปหัวใจเมื่อชี้ที่ปุ่ม */
        }

        input[type="submit"]:hover {
            background-color: #56baed; /* สีพื้นหลังปุ่ม submit เมื่อ hover */
        }
        .logo {
           
            font-size: 30px;
            font-weight: bold;
            margin-left: 15px;
        }

        .logo img {
            width: 250px;
            height: auto;
        }
    </style>
</head>
<body> 
<div class="logo"><img src="E-Borrow.png" alt="E-Borrow Logo"></div>
    
    <h1>เข้าสู่ระบบยืม - คืนอุปกรณ์</h1>
    
    <form action ="a_dashbord.php" method="post"> 
    <div class="form-outline mb-3">
    <label for="username">PSU Passport </label>
         <input type="text" name="frmLogin"> 
        
         <label for="password">Password</label>
          <input type="password" name="frmPwd" >
          <br>
            <input type="submit" value="Login">
            <div class="register-link">
            
          </div>
    
    </form>
</body>
</html>
 
    </form> 
    </body> 
</html>

</head>
<body>

   
  