<?php
include('db.php');
include('function.php');
// operation
if(isset($_POST["operation"]))
{
	// function อัปเดทข้อมูลอุปกรณ์
	if($_POST["operation"] == "approving")
	{
		$status = 'อนุมัติแล้ว';
		$stmt = $connection->prepare(
			"UPDATE borrowing_returning 
			SET username = :bp_username, first_name = :bp_first_name, last_name = :bp_last_name , list_name = :bp_list_name , 
            quantity = :bp_quantity, borrow_date = :bp_borrow_date, return_date = :bp_return_date, purpose_use = :bp_purpose_use, status = :bp_status  
			WHERE id = :bp_id
			"
		);
		$stmt->bindParam(':bp_username', $_POST["username"]);
		$stmt->bindParam(':bp_first_name', $_POST["first_name"]);
		$stmt->bindParam(':bp_last_name', $_POST["last_name"]);
		$stmt->bindParam(':bp_list_name', $_POST["list_name"]);
		$stmt->bindParam(':bp_quantity', $_POST["quantity"]);
		$stmt->bindParam(':bp_borrow_date', $_POST["borrow_date"]);
		$stmt->bindParam(':bp_return_date', $_POST["return_date"]);
		$stmt->bindParam(':bp_purpose_use', $_POST["purpose_use"]);
		$stmt->bindParam(':bp_status', $status);
        $stmt->bindParam(':bp_id', $_POST["id"]);
		$result = $stmt->execute();
		if(!empty($result))
		{
			echo 'อนุมัติการยืมสำเร็จแล้ว !';
		}
	}
}

?>