<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Kanit:wght@400;700&display=swap');

        body {
            font-family: 'Kanit', sans-serif;
            margin: 0;
            padding: 0;
        }

        .logo {
            font-size: 25px;
            font-weight: bold;
            margin-left: 15px;
        }

        .logo img {
            width: 70px;
            height: auto;
        }

        /* ส่วนของ Navbar */
        nav {
            background-color: #56baed;
            padding: 10px 0;
            color: #56baed;
            /* display: flex; */
            /* align-items: center; Align items vertically */
        }

        ul.navbar-nav li.nav-item {
            display: inline;
            margin-right: 20px;
        }

        a.nav-link {
            text-decoration: none;
            color: #56baed;
        }

        a.nav-link:hover {
            text-decoration: underline;
        }
		</style>
     <!-- เพิ่มตำแหน่งนี้ -->
     <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>

</head>

<body>
<div class="logo"><img src="E-Borrow.png" alt="E-Borrow Logo"></div>

<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="a_dashbord.php">หน้าหลัก</a>
        </li>
    
        <li class="nav-item">
          <a class="nav-link" href="report.php"> ออกรายงาน</a>
        </li>
       
          
          <li class="nav-item">
          <a class="nav-link" href="s_logout.php">ออกจากระบบ</a>
        </li>
        </li>
    </div>
</nav>
</head>
<!DOCTYPE html>
<html>

<style>

body {
            font-family: 'Kanit', sans-serif;
            margin: 0;
            padding: 0;
        }
</style>
<body>
	<div class="container box">
		<h1 align="center">ประวัติการยืม - คืนอุปกรณ์</h1>
		<br/>
		<div class="table-responsive">
		<br/>
			<table id="user_data" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th width="5%">ลำดับ</th>
						<th width="10%">รหัสนักศึกษา</th>
						<th width="10%">ชื่อ</th>
						<th width="10%">นามสกุล</th>
                        <th width="20%">ชื่อรายการอุปกรณ์</th>
						<th width="5%">จำนวน</th>
                        <th width="10%">วันที่ยืม</th>
						<th width="10%">วันที่คืน</th>
						<th width="10%">วัตถุประสงค์การใช้งาน</th>
                        <th width="10%">สถานะ</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.7.1.js"></script>
	<script src="https://cdn.datatables.net/2.0.1/js/dataTables.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdn.datatables.net/2.0.1/js/dataTables.bootstrap5.js"></script>
<script>
		// jQuery DataTable ดึงข้อมูลจากไฟล์ PHP ผ่าน Ajax แสดงข้อมูลในรูปแบบตาราง
		$(document).ready(function(){
		var dataTable = $('#user_data').DataTable({
			"processing":true,
			"serverSide":true,
			"order":[],
			"ajax":{
				url:"./a_history_datatable.php",
				type:"POST"
			},
			"columnDefs":[
				{
					"targets":[0, 3, 4],
					"orderable":false,
					
				},	
			],
		});
		
	});
	
	
	</script>
	
</body>
</html>
