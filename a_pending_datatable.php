<?php
// ค้นหารายการอุปกรณ์และแสดง
include('db.php');
include('function.php');
$query = '';
$output = array();
$query .= "SELECT * FROM borrowing_returning where status = 'รอดำเนินการ' ";
// if(isset($_POST["search"]["value"]))
// {
// 	$query .= 'WHERE username LIKE "%'.$_POST["search"]["value"].'%" ';
// }
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['column'].' ';
}
else
{
	$query .= 'ORDER BY id asc ';
}
if($_POST["length"] != -1)
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$stmt = $connection->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll();
$data = array();
$filtered_rows = $stmt->rowCount();
foreach($result as $row)
{
	$sub_array = array();
	$sub_array[] = $row["id"];
	$sub_array[] = $row["username"];
    $sub_array[] = $row["first_name"];
    $sub_array[] = $row["last_name"];
	$sub_array[] = $row["list_name"];
	$sub_array[] = $row["quantity"];
    $sub_array[] = $row["borrow_date"];
    $sub_array[] = $row["return_date"];
    $sub_array[] = $row["purpose_use"];
    $sub_array[] = $row["status"];
	$sub_array[] = '<button type="button" name="approving" id="'.$row["id"].'" class="btn btn-warning btn-xs approving">รอตรวจสอบ</button>';
	$data[] = $sub_array;
}
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);
echo json_encode($output);
?>