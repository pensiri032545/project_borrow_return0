﻿<?php
include("mysql_conn.php");

// create the SQL statement
$fname =  $_REQUEST['fname'];
$lname =  $_REQUEST['lname'];
$userLogin =  $_REQUEST['login'];
$hashedPwd = password_hash($_REQUEST['passwd'], PASSWORD_DEFAULT);
echo "Hashed password: " . $hashedPwd . "<br>";

$sql = "INSERT INTO users(ulogin,fname, lname,passwd) values(:bp_login, :bp_fname, :bp_lname, :bp_passwd)";
//Other techniques: https://www.sitepoint.com/community/t/try-catch-around-pdo-code-what-code-to-include/100765/5
//Transaction commit, rollback https://www.php.net/manual/en/pdo.lastinsertid.php
try {
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(':bp_login', $userLogin);
	$stmt->bindParam(':bp_fname', $fname);
	$stmt->bindParam(':bp_lname', $lname);
	$stmt->bindParam(':bp_passwd', $hashedPwd);
	$stmt->execute();

	echo $stmt->rowCount() . " records INSERTED"; 
} catch (PDOException $e) {		
	//https://stackoverflow.com/questions/10774943/check-for-duplicate-entry-vs-use-pdo-errorinfo-result
	echo "Insertion failed: ", $e->getMessage();

	if ($e->errorInfo[0] == 23000) {
		//if ($e->errorInfo[1] == 1062) {
		echo "Insertion failed due to duplicate entry";
	} else {
		echo "Insertion failed: ", $e->getMessage();
	}
	//Read more https://www.php.net/manual/en/class.pdoexception.php
} catch (Exception $e) {
	echo "General Error: The user could not be added.<br>".$e->getMessage();
}
?>