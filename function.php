<?php
include('db.php');

// 
function upload_image()
{
	if(isset($_FILES["image"]))
	{
		$extension = explode('.', $_FILES['image']['name']);
		$new_name = rand() . '.' . $extension[1];
		$destination = './upload/' . $new_name;
		move_uploaded_file($_FILES['image']['tmp_name'], $destination);
		return $new_name;
	}
}

//
function get_image_name($id)
{
	include('db.php');
	$stmt = $connection->prepare("SELECT image FROM equipment WHERE id = :bp_id");
	$stmt->bindParam(':bp_id', $_POST["id"]);
	$stmt->execute();
	$result = $stmt->fetchAll();
	foreach($result as $row)
	{
		return $row["image"];
	}
}

//
function get_total_all_records()
{
	include('db.php');
	$stmt = $connection->prepare("SELECT * FROM equipment");
	$stmt->execute();
	$result = $stmt->fetchAll();
	return $stmt->rowCount();
}

?>