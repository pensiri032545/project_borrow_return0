﻿<?php
$host = "localhost";
$user = "root";
$passwd = "";
$dbname = "project";
try {
      $conn = new PDO("mysql:host=$host; dbname=$dbname", $user, $passwd);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      //Read: https://www.php.net/manual/en/pdo.error-handling.php
      //echo "Connected successfully";

} catch(PDOException $e) {
	//echo "Connection failed: " . $e->getMessage();
}

?>