<?php
include('db.php');
include('function.php');
// operation
if(isset($_POST["operation"]))
{
	// function เพิ่มรายการอุปกรณ์
	if($_POST["operation"] == "Borrow")
	{
		
		$stmt = $connection->prepare("
		INSERT INTO borrowing_returning (username, first_name, last_name , list_name , quantity, borrow_date, return_date, purpose_use)
		VALUES (:bp_username, :bp_first_name, :bp_last_name , :bp_list_name , :bp_quantity, :bp_borrow_date, :bp_return_date, :bp_purpose_use)
		");
		$stmt->bindParam(':bp_username', $_POST["username"]);
		$stmt->bindParam(':bp_first_name', $_POST["first_name"]);
		$stmt->bindParam(':bp_last_name', $_POST["last_name"]);
		$stmt->bindParam(':bp_list_name', $_POST["list_name"]);
		$stmt->bindParam(':bp_quantity', $_POST["quantity"]);
		$stmt->bindParam(':bp_borrow_date', $_POST["borrow_date"]);
		$stmt->bindParam(':bp_return_date', $_POST["return_date"]);
		$stmt->bindParam(':bp_purpose_use', $_POST["purpose_use"]);
		$result = $stmt->execute();
		if(!empty($result))
		{
			echo 'ทำรายการยืมอุปกรณ์สำเร็จแล้ว !';
		}

	}
}

?>