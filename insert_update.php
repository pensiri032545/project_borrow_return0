<?php
include('db.php');
include('function.php');
// operation
if(isset($_POST["operation"]))
{
	// function เพิ่มรายการอุปกรณ์
	if($_POST["operation"] == "Create")
	{
		$image = '';
			if($_FILES["image"]["name"] != '')
				{
					$image = upload_image();
				}
		$stmt = $connection->prepare("
			INSERT INTO equipment (list_name, type, quantity, image) 
			VALUES (:bp_list_name, :bp_type, :bp_quantity, :bp_image)
		");
		$stmt->bindParam(':bp_list_name', $_POST["list_name"]);
			$stmt->bindParam(':bp_type', $_POST["type"]);
			$stmt->bindParam(':bp_quantity', $_POST["quantity"]);
			$stmt->bindParam(':bp_image', $image);
			$result = $stmt->execute();
		if(!empty($result))
		{
			echo 'เพิ่มรายการอุปกรณ์สำเร็จแล้ว !';
		}
	}

	// function อัปเดทข้อมูลอุปกรณ์
	if($_POST["operation"] == "Update")
	{
		$image = '';
		if($_FILES["image"]["name"] != '')
		{
			$image = upload_image();
		}
		else
		{
			$image = $_POST["hidden_image"];
		}
		$stmt = $connection->prepare(
			"UPDATE equipment 
			SET list_name = :bp_list_name, type = :bp_type, quantity = :bp_quantity, image = :bp_image  
			WHERE id = :bp_id
			"
		);
		$stmt->bindParam(':bp_list_name', $_POST["list_name"]);
		$stmt->bindParam(':bp_type', $_POST["type"]);
		$stmt->bindParam(':bp_quantity', $_POST["quantity"]);
		$stmt->bindParam(':bp_image', $image);
		$stmt->bindParam(':bp_id', $_POST["id"]);
		$result = $stmt->execute();
		if(!empty($result))
		{
			echo 'แก้ไขข้อมูลอุปกรณ์สำเร็จแล้ว !';
		}
	}
}

?>