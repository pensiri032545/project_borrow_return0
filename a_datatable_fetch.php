<?php
// ค้นหารายการอุปกรณ์และแสดง
include('db.php');
include('function.php');
$query = '';
$output = array();
$query .= "SELECT * FROM equipment ";
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE type LIKE "%'.$_POST["search"]["value"].'%" ';
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['column'].' ';
}
else
{
	$query .= 'ORDER BY id asc ';
}
if($_POST["length"] != -1)
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$stmt = $connection->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll();
$data = array();
$filtered_rows = $stmt->rowCount();
foreach($result as $row)
{
	$sub_array = array();
	$image = '';
	if($row["image"] != '')
	{
		$image = '<img src="upload/'.$row["image"].'" class="img-thumbnail" width="auto" height="auto" />';
	}
	else
	{
		$image = '';
	}
	$sub_array[] = $row["id"];
	$sub_array[] = $image;
	$sub_array[] = $row["list_name"];
	$sub_array[] = $row["type"];
	$sub_array[] = $row["quantity"];
	$sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="btn btn-warning btn-xs update">แก้ไข</button>';
	$sub_array[] = '<button type="button" name="delete" id="'.$row["id"].'" class="btn btn-danger btn-xs delete">ลบ</button>';
	$data[] = $sub_array;
}
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);
echo json_encode($output);
?>