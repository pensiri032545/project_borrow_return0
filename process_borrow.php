<?php
// กำหนดค่าการเชื่อมต่อ
$host = "localhost";
$user = "root";
$passwd = "";
$dbname = "project";

// สร้างการเชื่อมต่อ
$conn = new mysqli($host, $user, $passwd, $dbname);

// ตรวจสอบการเชื่อมต่อ
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// รับข้อมูลจากฟอร์ม
$username= $_POST["username"];
$first_name = $_POST["first_name"];
$last_name = $_POST["last_name"];
$list_name = $_POST["list_name"];
$quantity = $_POST["quantity"];
$borrow_date = $_POST["borrow_date"];
$return_date = $_POST["return_date"];
$purpose_use = $_POST["purpose_use"];

// เตรียมคำสั่ง SQL สำหรับการแทรกข้อมูล
$sql = "INSERT INTO borrowing_returning (username, first_name, last_name, list_name, quantity, borrow_date, return_date, purpose_use) VALUES ('$username', '$first_name', '$last_name', '$list_name', '$quantity', '$borrow_date', '$return_date', '$purpose_use')";

// ทำการแทรกข้อมูล
if ($conn->query($sql) === TRUE) {
    echo "Record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

// ปิดการเชื่อมต่อ
$conn->close();
?>
