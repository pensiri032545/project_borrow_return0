﻿<?php 
// session_start();
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if(empty($_POST['frmPwd']) || empty($_POST['frmLogin'])) {
	echo "<b>Please fill Login name and Password </b>";
	exit; 
} 
else { 
    //Collect the details and validate 
    $userLogin = $_REQUEST['frmLogin']; 
    $pwdInput = $_REQUEST['frmPwd']; 

    //Database connection
	include("mysql_conn.php");    

    $query = "select * from users where ulogin= :bp_login"; 
	
	/* Execute the query */
	try	{
  		$stmt = $conn->prepare($query);
  		$stmt->bindParam(':bp_login', $userLogin);
		$stmt->execute();
		
		$rowCnt = $stmt->rowCount();
		switch ($rowCnt) {
			case 0: {
				echo "ไม่พบผู้ใช้งาน <BR>"; 
				echo "<a href=\"s_index.php\">Login again</a>";
				break;
			} //end case 0 (No data found)
			case 1: {
				$row = $stmt ->fetch(PDO::FETCH_ASSOC);
				$uname = $row["fname"];
				$pwd = $row["passwd"];

				if (password_verify($pwdInput, $pwd)) {
					$counter = 1;
					echo "<font color = 'green'><b>Welcome $uname login successfully</b></font><br>";
					// create session for login and pass
					$_SESSION['login']= $userLogin;
					$_SESSION['uname']= $uname;
			
					 echo "SET Session to user : $uname, login : $userLogin <br>"; 
					 echo "<a href=\"dashbord1.php\">User detail</a><br>";
					echo "<a href=\"s_logout.php\">Logout</a>"; 
				}else{
					echo "<b>รหัสผ่านไม่ถูกต้อง<b><br>"; 
					echo "<a href=\"s_index.php\">Login again</a>"; 	
				} //end password_verify
				break;
			} // end case 1
			default: {
				echo "มีบัญชีผู้ใช้งาน <b> $userLogin <b> มากกว่า 1 บัญชี กรุณาแจ้งผู้ดูแลระบบให้ตรวจสอบข้อมูล<br>"; 
				echo "<a href=\"s_index.php\">Login again</a>"; 
				break;
			} // end case default
		}// end switch			
	} catch (PDOException $e) {		
		echo 'ขออภัย ไม่สามารถดึงข้อมูลจากฐานข้อมูลได้<br>';
		echo $e->getMessage();		
		echo "<a href=\"s_index.php\">Login again</a>";
	}	
	$conn = null;
}// end if (empty($_REQUEST['login'])|| empty($_REQUEST['passwd']))
?>